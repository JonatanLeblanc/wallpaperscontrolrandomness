#include <string>
#include <vector>

class ImageFormat
{
	using string_type = std::string;
	using vectorString = std::vector<string_type>;
private:
	vectorString m_imageFormats = { ".jpg", ".jpeg", ".png", ".bmp" };
public:
	bool IsValidImage(string_type filename);
};

