#include "ImageFormat.h"

bool ImageFormat::IsValidImage(string_type filename)
{
	std::size_t pos = filename.find(".");
	if (pos != string_type::npos) {
		string_type fileExt = filename.substr(pos);
		for (auto imgFormat : m_imageFormats) {
			if (fileExt == imgFormat) {
				return true;
			}
		}
	}
	return false;
}
