#include "Playlist.h"

void Playlist::SetPlaylist(vectorImage images, uint rating)
{
	m_images = images;
	m_rating = rating;
}

Playlist::Playlist(vectorImage images, uint rating)
{
	SetPlaylist(images, rating);
}

Playlist::Playlist(uint rating)
{
	m_images = vectorImage();
	m_rating = rating;
}

std::string Playlist::GetImagePath(uint index)
{
	return (*GetImage(index)).GetPath();
}

std::shared_ptr<Image> Playlist::GetImage(uint index)
{
	return m_images.at(index);
}

unsigned int Playlist::GetLength()
{
	return m_images.size();
}

std::vector<unsigned int> Playlist::GetRatings()
{
	std::vector<uint> ratings = std::vector<uint>();
	ratings.reserve(m_images.size());

	for (auto image : m_images) {
		ratings.push_back(image->GetRating());
	}

	return ratings;
}

void Playlist::AddImage(std::shared_ptr<Image> image)
{
	m_images.push_back(image);
}
