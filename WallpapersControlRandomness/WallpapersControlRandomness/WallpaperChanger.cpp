#include "WallpaperChanger.h"

void WallpaperChanger::SetPlaylist(std::shared_ptr<Playlist> playlist)
{
	m_playlist = playlist;
}

unsigned int WallpaperChanger::GenerateRandomNumber()
{
	std::random_device randDev;
	std::mt19937 randomNumberGenerator(randDev());

	std::vector<unsigned int> ratings = m_playlist->GetRatings();
	std::discrete_distribution<uint> dist(std::begin(ratings), std::end(ratings));

	return dist(randomNumberGenerator);
}

void WallpaperChanger::Start()
{
	for (;;) {
		uint index = GenerateRandomNumber();
		System().SetWallpaper((*m_playlist).GetImagePath(index));
		m_currentImage = (*m_playlist).GetImage(index);
		std::this_thread::sleep_for(std::chrono::seconds(m_currentImage->GetTime()));
	}
}

WallpaperChanger::WallpaperChanger()
{
	m_currentImage = nullptr;
	m_playlist = nullptr;
}

WallpaperChanger::WallpaperChanger(std::shared_ptr<Playlist> playlist)
{
	SetPlaylist(playlist);
}

void WallpaperChanger::CreatePlaylistFromFolder(string_type folderPath)
{
	uint playlistRating = 2;
	m_playlist = std::shared_ptr<Playlist>(new Playlist(playlistRating));

	std::vector<string_type> imagesFilePath = System().FindImagesInFolder(folderPath);
	uint rating = 2;
	uint timeSec = 5;

	for (auto imagePath : imagesFilePath) {
		std::shared_ptr<Image> img = std::shared_ptr<Image>(new Image(imagePath, timeSec, rating));
		m_playlist->AddImage(img);
	}
}
