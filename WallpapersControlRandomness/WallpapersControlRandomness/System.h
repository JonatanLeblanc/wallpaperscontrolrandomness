#include <Windows.h>
#include <filesystem>
#include "ImageFormat.h"

class System
{
	using string_type = std::string;
	using vectorString = std::vector<string_type>;
public:
	int static SetWallpaper(string_type imagePath);
	vectorString static FindImagesInFolder(string_type folderPath);
};
