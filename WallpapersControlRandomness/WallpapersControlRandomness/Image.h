#include <string>

class Image
{
	using string_type = std::string;
	using uint = unsigned int;
private:
	string_type m_filePath;
	uint m_timeInSec;
	uint m_rating;

	void SetImage(string_type filePath, uint timeInSec, uint rating);
public:
	Image(string_type filePath, uint timeInSec, uint rating);
	string_type GetPath() { return m_filePath; }
	uint GetTime() { return m_timeInSec; }
	uint GetRating() { return m_rating; }
};

