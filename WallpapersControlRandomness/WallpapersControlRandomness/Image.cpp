#include "Image.h"

void Image::SetImage(string_type filePath, uint timeInSec, uint rating)
{
	m_filePath = filePath;
	m_timeInSec = timeInSec;
	m_rating = rating;
}

Image::Image(string_type filePath, uint timeInSec, uint rating)
{
	SetImage(filePath, timeInSec, rating);
}
