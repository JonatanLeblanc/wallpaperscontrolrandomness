#include "System.h"
#include "Playlist.h"
#include <thread>
#include <chrono>
#include <random>

class WallpaperChanger
{
	using string_type = std::string;
	using uint = unsigned int;
private:
	std::shared_ptr<Playlist> m_playlist;
	std::shared_ptr<Image> m_currentImage;

	void SetPlaylist(std::shared_ptr<Playlist> playlist);
	uint GenerateRandomNumber();
public:
	[[ noreturn ]] void Start();
	WallpaperChanger();
	WallpaperChanger(std::shared_ptr<Playlist> playlist);
	void CreatePlaylistFromFolder(string_type folderPath);
};

