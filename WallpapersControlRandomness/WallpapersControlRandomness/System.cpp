#include "System.h"

int System::SetWallpaper(string_type imagePath)
{
	return SystemParametersInfoA(SPI_SETDESKWALLPAPER, 0, (PVOID)imagePath.c_str(), SPIF_UPDATEINIFILE);
}

std::vector<std::string> System::FindImagesInFolder(string_type folderPath)
{
	ImageFormat imgFormat = ImageFormat();

	namespace fs = std::filesystem;
	const fs::path PathToFolder{ folderPath };
	vectorString imageFiles = vectorString();

	for (const auto& entry : fs::directory_iterator(PathToFolder)) {
		const string_type fileName = entry.path().filename().string();
		if (entry.is_regular_file() && imgFormat.IsValidImage(fileName)) {
			imageFiles.push_back(folderPath + fileName);
		}
	}
	return imageFiles;
}
