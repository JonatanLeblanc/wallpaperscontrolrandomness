#include "Image.h"
#include <vector>
#include <memory>

class Playlist
{
	using uint = unsigned int;
	using int_type = int;
	using vectorImage = std::vector<std::shared_ptr<Image>>;
	using string_type = std::string;
	using vectorUint = std::vector<uint>;

private:
	uint m_rating;
	vectorImage m_images;

	void SetPlaylist(vectorImage images, uint rating);
public:
	Playlist(vectorImage images, uint rating);
	Playlist(uint rating);
	string_type GetImagePath(uint index);
	std::shared_ptr<Image> GetImage(uint index);
	uint GetLength();
	vectorUint GetRatings();
	void AddImage(std::shared_ptr<Image> image);
};

